# HM ESE IOT

Base repository containing all repositories required for the HM ESE IOT classes

## Use of submodules

This repository is using nested submodules. Submodules are repositories that are included in the base repository and are complementary. Submodule repositories are maintained elsewhere and shall be pulled separately after cloning this repository. 

To clone this repository including its submodules you can use the ```--recursive``` parameter.

```
git clone --recursive [URL to Git repo]
```

If you already have cloned the repository and now want to load it’s submodules you have to use submodule update.

```
git submodule update --init
# if there are nested submodules:
git submodule update --init --force --remote
```

# Disclaimer
The content of this repository is provided in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
# License
This repository is part of the IoT classes delivered at HAN Engineering ELT.
This repository is free: You may redistribute it and/or modify it under the terms of a Creative  Commons Attribution-NonCommercial 4.0 International License  (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.